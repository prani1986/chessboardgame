# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* It is chess board application created in react-native
* 8 * 8 grid in the screen center
* Version - 1.0.0

### How do I get set up? ###


     * First Clone repository and do following configuration.
	 * After cloning repostory do 
	 * npm install / yarn install

* Configuration

    - Node: 10.19.0
   	- Yarn: 1.22.4
   	- npm: 6.14.3
   	- Watchman: 4.9.0
   	- CocoaPods: 1.10.0
	
* Dependencies
	- react: 16.13.1
	- react-native: 0.63.3
	
* How to run tests

    - yarn test --coverage
    - yarn jest -- -u
  
* Deployment instructions

    - npx react-native run-ios --simulator="iPhone 11" - to run on ios
    - npx react-native start --reset-cache (in one terminal)
    - npx react-native run-ios - to run on android (First keep open an emulator)

